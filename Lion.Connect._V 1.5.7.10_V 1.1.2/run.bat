@echo off
@rem Batch file to start application from command shell
@rem
@rem Possible helpful JavaVM options:
@rem
@rem 1. Remote debugging by NetBeans
@rem       	-agentlib:jdwp=transport=dt_socket,server=y,address=8000,suspend=n
@rem
@rem 2. Allow remote CPU sampling and JMX-connection by jvisualvm.exe
@rem       	-Dcom.sun.management.jmxremote.port=9010 ^
@rem        -Dcom.sun.management.jmxremote.local.only=false ^
@rem        -Dcom.sun.management.jmxremote.authenticate=false ^
@rem        -Dcom.sun.management.jmxremote.ssl=false
@rem 
@rem    In case of CPU sampling please don't forget to start on application side the service:
@rem       	"C:\Programme\java\jdk1.7.0_51\bin>jstatd -p1234 -J-Djava.security.policy=tools.policy"
@rem    and create before the file C:\Programme\java\jdk1.7.0_51\bin\tools.policy with the content:
@rem    	grant codebase "file:${java.home}/../lib/tools.jar" {
@rem    	   permission java.security.AllPermission;
@rem    	};
@rem
@rem 3. Allow that remote NetBeans profiler can attach to this application
@rem        -agentpath:<remote>\lib\deployed\jdk16\windows\profilerinterface.dll=<remote>\lib,5140
@rem
@rem        Replace the placeholder '<remote>' by the path where you have installed the remote 
@rem        profiling pack (see NB menu 'Profile/Attach Profiler.../Attach mode')


lib\jdk\bin\java ^
-Xmx512M ^
-noverify ^
-Djava.library.path="lib" ^
-Djava.util.logging.config.file=data/logger.properties ^
-DlayoutStyle=Seidenader ^
-DglobalStyle=style2_2 ^
-Dcom.sun.jndi.ldap.object.disableEndpointIdentification=true ^
-Djava.security.krb5.conf=data/krb5.conf ^
-Djava.security.auth.login.config=data/jaas.conf ^
-Djava.security.krb5.realm= ^
-Djava.security.krb5.kdc= ^
-Djava.security.krb5.debug=true ^
-Djava.naming.factory.initial=com.sun.jndi.ldap.LdapCtxFactory ^
-Djava.naming.provider.url= ^
-jar MedipakLms.jar -t -s %*
