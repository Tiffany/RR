@echo on

SET AppPath=%~dp0%..

echo "path is " %AppPath%

cd %AppPath%\libs\exe\nssm\win64

nssm install mbx2PackMlConverter "%AppPath%\runConverter.bat"

nssm set mbx2PackMlConverter DisplayName MbxToPackMlConverter
nssm set mbx2PackMlConverter Description Mbx to PackMl Converter
nssm set mbx2PackMlConverter Start SERVICE_AUTO_START

pause

nssm start mbx2PackMlConverter

cmd /k