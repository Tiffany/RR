<?xml version='1.0' encoding='UTF-8'?> 
<!--
Copyright (c) 2021
Dividella AG a Koerber Group company, 9472 Grabs, Switzerland
koerber-pharma.com

Describes the XML schema of all parts in the file lionCfg.xml 
which belongs exclusively to the Lion module "PasX".
-->

<xs:schema version="1.0"
           targetNamespace="http://koerber.com/Lion/SamiImporter" 
           xmlns="http://koerber.com/Lion/SamiImporter" 
           xmlns:xs="http://www.w3.org/2001/XMLSchema" 
           xmlns:cr="http://koerber.com/Lion/Core" 
           elementFormDefault="qualified">
	
    <xs:import schemaLocation="coreCfg.xsd"
               namespace="http://koerber.com/Lion/Core"/>

    <xs:element name="samiImporterModule">
        <xs:annotation>
            <xs:documentation>Top-most configuration of the module.</xs:documentation>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="schemaVersion" type="cr:schemaVersionType"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    
    <xs:element name="samiImporter4Unit">
        <xs:annotation>
            <xs:documentation>General PAS-X settings for level 'Unit'.</xs:documentation>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="saBridge" type="saBridgeType">
                    <xs:annotation>
                        <xs:documentation>This element contains configuration for
                            one Seidenader SAMI SQL to MBX/MSI bridge.</xs:documentation>
                    </xs:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>

    <xs:complexType name="saBridgeType">
        <xs:sequence>
            <xs:element name="alarmEventMode" type="alarmEventModeType" default="ACTIVATION">
                <xs:annotation>
                    <xs:documentation>Determines whether to send events on activaiton or
                        deactivation.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="alarmDescription" type="xs:string" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>Configuration for the description message template for alarms.
                        The supported placeholders for the alarm description are: '{RemoteInterfaceNumber}', '{CritEvtCount}', '{Description}', '{ResetTime}'.
                        The default description is: '{RemoteInterfaceNumber} {CritEvtCount} "{Description}" "{ResetTime}"'.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="alarmLevelMappings" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>Contains all alarm levels that will result in an
                        MsiExceptionMessage to be sent and the exceptionType
                        that will be used. If for a given event with a given
                        alarm level no matching mapping is found, the event
                        is not sent to MES via MsiExceptionMessage.
                    </xs:documentation>
                </xs:annotation>
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="alarmLevelMapping" type="alarmLevelMappingType" minOccurs="0" maxOccurs="unbounded">
                            <xs:annotation>
                                <xs:documentation>Configuration for one mapping of an alarm
                                    level to an exception type.</xs:documentation>
                            </xs:annotation>
                        </xs:element>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
            <xs:element name="auditTrailDescription" type="xs:string" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>Configuration for the description message for audit trail(critical parameter).
                        The supported placeholders for the audit trail description are: '{RemoteInterfaceNumber}', '{CritEvtCount}', '{Name}', '{OldValue}', '{NewValue}'.
                        The default description is: '{Name} Changed From: {OldValue} To: {NewValue}'.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="sql" type="sqlType">
                <xs:annotation>
                    <xs:documentation>This element contains the configuration for the
                        SQL-database interface for Seidenader.</xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="alarmLevelMappingType">
        <xs:sequence>
            <xs:element name="alarmLevel" type="alarmLevelType">
                <xs:annotation>
                    <xs:documentation>The alarm level to map to an exception type.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="exceptionType" type="exceptionTypeType">
                <xs:annotation>
                    <xs:documentation>The exception type to send for the given alarm
                        level. Typical values are:
                        MSI Alarm
                        MSI Info</xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="sqlType">
        <xs:sequence>
            <xs:element name="databaseUrl">
                <xs:annotation>
                    <xs:documentation>The JDBC URL of the SQL database server for
                        Seidenader (e.g. "jdbc:postgresql://localhost:5432/sami").</xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:minLength value="1"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="driverName">
                <xs:annotation>
                    <xs:documentation>Necessary JDBC driver for SQL connection, for instance:
                        org.postgresql.Driver</xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:minLength value="1"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="userName" type="xs:string">
                <xs:annotation>
                    <xs:documentation>User name for database connection.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="password" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Password for database connection.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="cycleTime" type="xs:nonNegativeInteger" minOccurs="0" default="1000">
                <xs:annotation>
                    <xs:documentation>Optional cycle time for database polling [ms] (1..n).
                        Default is 1000ms.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="schema" type="xs:string" default="sami_osi_pi">
                <xs:annotation>
                    <xs:documentation>Name of the Seidenader DB schema.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="alarmColumns" type="alarmColumnsType">
                <xs:annotation>
                    <xs:documentation>Configuration for all columns required for fetching
                        alarm event data from DB.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="auditTrailColumns" type="auditTrailColumnsType">
                <xs:annotation>
                    <xs:documentation>Configuration for all columns required for fetching
                        audit trail data from DB.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="mesBatchOperators" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>List of batch operator names to filter for. The operator
                        name is used to identify batches created by the MES.
                        If at least one batch operator name is configured, then
                        only those events whose batch object has an operator set
                        that matches at least one of the configured names, will
                        be transmitted via MSI - all others will only be marked
                        for deletion.</xs:documentation>
                </xs:annotation>
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="mesBatchOperator" type="xs:string" minOccurs="0" maxOccurs="unbounded">
                            <xs:annotation>
                                <xs:documentation>One batch operator name. Only records that belong
                                    to a batch whose operator name matches at least
                                    one configured batch operator will be included.
                                    Please note, that this can be a literal operator
                                    name, or but an expression potentially matching
                                    multiple possible values by emplying SQL wildcard
                                    characters ('%', '_').</xs:documentation>
                            </xs:annotation>
                        </xs:element>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
            <xs:element name="auditTrailRelevanceValues" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>List of audit trail relevance values to filter for. The relevance
                        values are used to identify relevant audit trail data.
                        If at least one relevance value is configured, then
                        only those audit trail records
                        that matches at least one of the configured values, will
                        be transmitted via MSI - all others will only be marked
                        for deletion.</xs:documentation>
                </xs:annotation>
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="auditTrailRelevanceValue" type="xs:string" minOccurs="0" maxOccurs="unbounded">
                            <xs:annotation>
                                <xs:documentation>One audit trail relevance value. Only records that
                                    matches at least one configured values will be included.
                                    Please note, that this can be a literal operator
                                    name, or but an expression potentially matching
                                    multiple possible values by emplying SQL wildcard
                                    characters ('%', '_').</xs:documentation>
                            </xs:annotation>
                        </xs:element>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="commonColumnsType" abstract="true">
        <xs:sequence>
            <xs:element name="batchNumber" type="xs:string" minOccurs="0" default="pi_interface_batchdata.bd_batchnumber">
                <xs:annotation>
                    <xs:documentation>Column name (with table name prefixes, but without
                        the schema prefixed) for the column that contains
                        the batch number.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="batchOperator" type="xs:string" minOccurs="0" default="pi_interface_batchdata.bd_operator">
                <xs:annotation>
                    <xs:documentation>Column name (with table name prefixes, but without
                        the schema prefixed) for the column that contains
                        the name of operator that created the batch.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="sfoId" type="xs:string" minOccurs="0" default="pi_interface_batchdata.bd_desc1">
                <xs:annotation>
                    <xs:documentation>Column name (with table name prefixes, but without
                        the schema prefixed) for the column that contains
                        the shopfloor order id.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="equipmentId" type="xs:string" minOccurs="0" default="">
                <xs:annotation>
                    <xs:documentation>Column name (with table name prefixes, but without
                        the schema prefixed) for the column that contains
                        the optional equipment id. Not that this is not
                        the id used in the description texts. Currently
                        not used.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="critEvtCount" type="xs:string" minOccurs="0" default="">
                <xs:annotation>
                    <xs:documentation>Column name (with table name prefixes, but without
                        the schema prefixed) for the column that contains
                        the optional critical events counter value.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="userComment" type="xs:string" minOccurs="0" default="">
                <xs:annotation>
                    <xs:documentation>Column name (with table name prefixes, but without
                        the schema prefixed) for the column that contains
                        the optional user comment. Currently not used.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="user" type="xs:string" minOccurs="0" default="">
                <xs:annotation>
                    <xs:documentation>Column name (with table name prefixes, but without
                        the schema prefixed) for the column that contains
                        the optional user name. Currently not used.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="deviationNumber" type="xs:string" minOccurs="0" default="">
                <xs:annotation>
                    <xs:documentation>Column name (with table name prefixes, but without
                        the schema prefixed) for the column that contains
                        the optional deviation number. Currently not used.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="manufacturingOrderID" type="xs:string" minOccurs="0" default="">
                <xs:annotation>
                    <xs:documentation>Column name (with table name prefixes, but without
                        the schema prefixed) for the column that contains
                        the optional manufacturing order id. Currently not
                        used.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="productionUnit" type="xs:string" minOccurs="0" default="">
                <xs:annotation>
                    <xs:documentation>Column name (with table name prefixes, but without
                        the schema prefixed) for the column that contains
                        the optional production unit name. Currently not
                        used.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="operationID" type="xs:string" minOccurs="0" default="">
                <xs:annotation>
                    <xs:documentation>Column name (with table name prefixes, but without
                        the schema prefixed) for the column that contains
                        the optional operation id. Currently not used.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="stepID" type="xs:string" minOccurs="0" default="">
                <xs:annotation>
                    <xs:documentation>Column name (with table name prefixes, but without
                        the schema prefixed) for the column that contains
                        the optional step id. Currently not used.</xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="alarmColumnsType">
        <xs:complexContent>
            <xs:extension base="commonColumnsType">
                <xs:sequence>
                    <xs:element name="id" type="xs:string" minOccurs="0" default="pi_interface_alarmdata.ad_group_id">
                        <xs:annotation>
                            <xs:documentation>Column name (with table name prefixes, but without
                                the schema prefixed) for the column that contains
                                the unique id of the alarm event record.</xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="parentGroupId" type="xs:string" minOccurs="0" default="pi_interface_alarmdata.ad_parent_group_id">
                        <xs:annotation>
                            <xs:documentation>Column name (with table name prefixes, but without
                                the schema prefixed) for the column that contains
                                the parent group id.</xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="description" type="xs:string" minOccurs="0" default="pi_interface_alarmdata.ad_alarmdescription">
                        <xs:annotation>
                            <xs:documentation>Column name (with table name prefixes, but without
                                the schema prefixed) for the column that contains
                                the human readable alarm description text.</xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="alarmId" type="xs:string" minOccurs="0" default="pi_interface_alarmdata.ad_alarmid">
                        <xs:annotation>
                            <xs:documentation>Column name (with table name prefixes, but without
                                the schema prefixed) for the column that contains
                                the optional alarmId. Currently not used.</xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="alarmIn" type="xs:string" minOccurs="0" default="pi_interface_alarmdata.ad_alarmin">
                        <xs:annotation>
                            <xs:documentation>Column name (with table name prefixes, but without
                                the schema prefixed) for the column that contains
                                the alarm activation timestamp.</xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="alarmOut" type="xs:string" minOccurs="0" default="pi_interface_alarmdata.ad_alarmout">
                        <xs:annotation>
                            <xs:documentation>Column name (with table name prefixes, but without
                                the schema prefixed) for the column that contains
                                the alarm deactivation timestamp.</xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="alarmLevel" type="xs:string" minOccurs="0" default="pi_interface_alarmdata.ad_alarmlevel">
                        <xs:annotation>
                            <xs:documentation>Column name (with table name prefixes, but without
                                the schema prefixed) for the column that contains
                                the alarm level.</xs:documentation>
                        </xs:annotation>
                    </xs:element>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <xs:complexType name="auditTrailColumnsType">
        <xs:complexContent>
            <xs:extension base="commonColumnsType">
                <xs:sequence>
                    <xs:element name="id" type="xs:string" minOccurs="0" default="pi_interface_audittraildata.at_tabid">
                        <xs:annotation>
                            <xs:documentation>Column name (with table name prefixes, but without
                                the schema prefixed) for the column that contains
                                the unique id of the audit trail record.</xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="name" type="xs:string" minOccurs="0" default="pi_interface_audittraildata.at_name">
                        <xs:annotation>
                            <xs:documentation>Column name (with table name prefixes, but without
                                the schema prefixed) for the column that contains
                                the audit trail name(parameter text for critical parameters).</xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="oldValue" type="xs:string" minOccurs="0" default="pi_interface_audittraildata.at_oldvalue">
                        <xs:annotation>
                            <xs:documentation>Column name (with table name prefixes, but without
                                the schema prefixed) for the column that contains
                                the audit trail old value.</xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="newValue" type="xs:string" minOccurs="0" default="pi_interface_audittraildata.at_newvalue">
                        <xs:annotation>
                            <xs:documentation>Column name (with table name prefixes, but without
                                the schema prefixed) for the column that contains
                                the audit trail new value.</xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="relevance" type="xs:string" minOccurs="0" default="pi_interface_audittraildata.at_relevance">
                        <xs:annotation>
                            <xs:documentation>Column name (with table name prefixes, but without
                                the schema prefixed) for the column that contains
                                the audit trail relevance value.</xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="timeEvent" type="xs:string" minOccurs="0" default="pi_interface_audittraildata.at_timeevent">
                        <xs:annotation>
                            <xs:documentation>Column name (with table name prefixes, but without
                                the schema prefixed) for the column that contains
                                the audit trail timestamp.</xs:documentation>
                        </xs:annotation>
                    </xs:element>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <xs:simpleType name="alarmEventModeType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="ACTIVATION" />
            <xs:enumeration value="DEACTIVATION" />
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="alarmLevelType">
        <xs:restriction base="xs:string">
            <xs:pattern value="\d+|"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="exceptionTypeType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="MSI Alarm"/>
            <xs:enumeration value="MSI Info"/>
            <xs:enumeration value="MSI Critical Parameter"/>
        </xs:restriction>
    </xs:simpleType>

</xs:schema>
