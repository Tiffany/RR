@echo off
@rem Batch file to start application in GUI-less mode from command shell.
@rem
@rem Possible helpfull options:
@rem - remote debugging: -agentlib:jdwp=transport=dt_socket,server=y,address=8000,suspend=n

lib\jdk\bin\javaw ^
-Xmx512M ^
-noverify ^
-agentlib:jdwp=transport=dt_socket,server=y,address=8000,suspend=n ^
-Dcom.sun.jndi.ldap.object.disableEndpointIdentification=true ^
-Djava.util.logging.config.file=data/logger.properties ^
-Djava.security.krb5.conf=data/krb5.conf ^
-Djava.security.auth.login.config=data/jaas.conf ^
-Djava.naming.factory.initial=com.sun.jndi.ldap.LdapCtxFactory ^
-jar MedipakLms.jar -n