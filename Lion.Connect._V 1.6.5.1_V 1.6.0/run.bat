@echo off

@rem Batch file to start application from command shell

@rem Possible customer or developer settings
@rem -------------------------------------------------------------------------
@rem 1. Display the associated console window.
@rem	    lib\jdk\bin\java ^  instead of  lib\jdk\bin\javaw ^

@rem 2. Deactivate the LION splashscreen (during the boot-up) as top level window.
@rem 	Usage of the -s command line argument.
@rem 	    -jar LionSuite.jar -s %*	 

@rem 3. Integration of the LION in a window. 
@rem 	Gives the possibility to move the application like a normal Windows application.
@rem    Usage of the -t command line argument.
@rem 	    -jar LionSuite.jar -t %*

@rem 3. Start the LION in demo (simulated) mode. No modification needed in lmsCfg.xml file
@rem 	The LION starts with all packML connections in SIMULATED operation mode.
@rem 	The LION starts with all pasX MBX and pasX Soap (if used) connections in SIMULATED operation mode.
@rem    The LION starts with a local user management.
@rem    The LION starts in a frame like a normal Windows application.
@rem    Usage of the -d command line argument.
@rem 	    -jar LionSuite.jar -d %*

@rem 4. Many arguments can be used in the same command line
@rem 	    -jar LionSuite.jar -t -s %*

@rem Possible helpful JavaVM options:
@rem -------------------------------------------------------------------------
@rem 1. Remote debugging by NetBeans
@rem        -agentlib:jdwp=transport=dt_socket,server=y,address=8000,suspend=n

@rem 2. Allow remote CPU sampling and JMX-connection by jvisualvm.exe
@rem        -Dcom.sun.management.jmxremote.port=9010 ^
@rem        -Dcom.sun.management.jmxremote.local.only=false ^
@rem        -Dcom.sun.management.jmxremote.authenticate=false ^
@rem        -Dcom.sun.management.jmxremote.ssl=false
 
@rem    In case of CPU sampling please don't forget to start on application side the service:
@rem        "C:\Programme\java\jdk1.7.0_51\bin>jstatd -p1234 -J-Djava.security.policy=tools.policy"
@rem        and create before the file C:\Programme\java\jdk1.7.0_51\bin\tools.policy with the content:
@rem        grant codebase "file:                            $                {java.home}            /../lib/tools.jar" {
@rem    	    permission java.security.AllPermission;
@rem        };

@rem 3. Allow that remote NetBeans profiler can attach to this application
@rem        -agentpath:<remote>\lib\deployed\jdk16\windows\profilerinterface.dll=<remote>\lib,5140
@rem
@rem        Replace the placeholder '<remote>' by the path where you have installed the remote 
@rem        profiling pack (see NB menu 'Profile/Attach Profiler.../Attach mode')



:START_APPLICATION

libs\jdk\bin\java ^
-classpath "libs/classpath/*" ^
--module-path libs/modules ^
-Djava.library.path="libs/native" ^
-Xmx2048M ^
-XX:+HeapDumpOnOutOfMemoryError ^
-XX:HeapDumpPath=data/lion/log/ ^
-noverify ^
-Dderby.stream.error.file=data/lion/log/derby.log ^
-Dqueue.defaultMaximumQueueDepthMessages=60 ^
-Dqueue.defaultOverflowPolicy=RING ^
-Djava.util.logging.config.file=config/lion/logger.properties ^
-Djava.security.krb5.conf=config/lion/krb5.conf ^
-Djava.security.krb5.realm=EADOM.CH ^
-Djava.security.krb5.kdc=diviserv80.EAdom.ch ^
-Djava.security.krb5.debug=true ^
-Djava.security.auth.login.config=config/lion/jaas.conf ^
-Dcom.sun.jndi.ldap.object.disableEndpointIdentification=true ^
-Dorg.eclipse.jetty.util.log.class=org.eclipse.jetty.util.log.StdErrLog ^
-Dorg.eclipse.jetty.LEVEL=INFO ^
-DlayoutStyle=Seidenader ^
-DglobalStyle=style2_2 ^
-DbrowserRemoteDebugging=disable ^
-Dsun.java2d.uiScale=1.0 ^
--add-opens java.base/java.time=com.google.gson ^
--add-modules com.koerber.lion.customer ^
--add-reads com.koerber.lion.customer=ALL-UNNAMED ^
--add-reads at.alphagate.asphere.runtime=ALL-UNNAMED ^
--add-modules ALL-MODULE-PATH ^
--add-reads com.koerber.lion.core_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.core_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.packml_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.pasx_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.suite=ALL-UNNAMED ^
--add-reads com.koerber.lion.batch_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.batch_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.eventbroker_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.eventbroker_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.eventlog_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.eventlog_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.digitaltwin_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.digitaltwin_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.webserver_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.user_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.user_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.webserver_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.format_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.format_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.formatpart_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.formatpart_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.eventimport_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.eventimport_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.i18n_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.i18n_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.kpi_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.kpi_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.log_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.log_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.packml_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.pasx_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.workflow_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.lineclearance_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.lineclearance_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.portal_runtime=ALL-UNNAMED ^
--add-reads com.koerber.lion.portal_common=ALL-UNNAMED ^
--add-reads com.koerber.lion.shared_runtime=ALL-UNNAMED ^
--module com.koerber.lion.suite/de.medipak.lms.Main -s -t %*  
            
IF %ERRORLEVEL%==0 GOTO SHUTDOWN_APP
IF %ERRORLEVEL%==2 GOTO START_APPLICATION

GOTO END

:SHUTDOWN_APP
 echo.
 echo ***********************************************************
 echo * Application exit                                        *
 echo ***********************************************************
 GOTO END

:END
:END