<?xml version="1.0" encoding='UTF-8'?>
<!--
Copyright (c) 2021
Dividella AG a Koerber Group company, 9472 Grabs, Switzerland
koerber-pharma.com

Describes the XML schema of the file lionCfg.xml on the top-most level.
-->

<xs:schema version="1.0"
           targetNamespace="http://koerber.com/Lion" 
           xmlns="http://koerber.com/Lion"  
           xmlns:cr="http://koerber.com/Lion/Core"
           xmlns:eb="http://koerber.com/Lion/EventBroker"
           xmlns:in="http://koerber.com/Lion/I18n"
           xmlns:usr="http://koerber.com/Lion/User"
           xmlns:el="http://koerber.com/Lion/EventLog"
           xmlns:pm="http://koerber.com/Lion/PackML"
           xmlns:dt="http://koerber.com/Lion/DigitalTwin"
           xmlns:ei="http://koerber.com/Lion/EventImport"
           xmlns:fp="http://koerber.com/Lion/FormatPart"
           xmlns:ft="http://koerber.com/Lion/Format" 
           xmlns:bt="http://koerber.com/Lion/Batch" 
           xmlns:wf="http://koerber.com/Lion/Workflow"
           xmlns:kpi="http://koerber.com/Lion/KPI"
           xmlns:px="http://koerber.com/Lion/PasX"
           xmlns:dv="http://koerber.com/Lion/DeltaV"
           xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">


    <xs:import schemaLocation="coreCfg.xsd"
               namespace="http://koerber.com/Lion/Core"/>
    <xs:import schemaLocation="i18nCfg.xsd"
               namespace="http://koerber.com/Lion/I18n"/>
    <xs:import schemaLocation="eventBrokerCfg.xsd"
               namespace="http://koerber.com/Lion/EventBroker"/>
    <xs:import schemaLocation="userCfg.xsd"
               namespace="http://koerber.com/Lion/User"/>
    <xs:import schemaLocation="eventLogCfg.xsd"
               namespace="http://koerber.com/Lion/EventLog"/>
    <xs:import schemaLocation="packMlCfg.xsd"
               namespace="http://koerber.com/Lion/PackML"/>
    <xs:import schemaLocation="digitalTwinCfg.xsd"
               namespace="http://koerber.com/Lion/DigitalTwin"/>
    <xs:import schemaLocation="eventImportCfg.xsd"
               namespace="http://koerber.com/Lion/EventImport"/>
    <xs:import schemaLocation="formatPartCfg.xsd"
               namespace="http://koerber.com/Lion/FormatPart"/>
    <xs:import schemaLocation="formatCfg.xsd"
               namespace="http://koerber.com/Lion/Format"/>
    <xs:import schemaLocation="batchCfg.xsd"
               namespace="http://koerber.com/Lion/Batch"/>
    <xs:import schemaLocation="workflowCfg.xsd"
               namespace="http://koerber.com/Lion/Workflow"/>
    <xs:import schemaLocation="pasXCfg.xsd"
               namespace="http://koerber.com/Lion/PasX"/>
    <xs:import schemaLocation="kpiCfg.xsd"
               namespace="http://koerber.com/Lion/KPI"/>
    <xs:import schemaLocation="deltaVCfg.xsd"
               namespace="http://koerber.com/Lion/DeltaV"/>

    <xs:element name="lionCfg">
        <xs:complexType>
            <xs:sequence>
                
                <xs:element name="schemaVersion" type="cr:schemaVersionType"/>

                <xs:element name="contentVersion">
                    <xs:complexType>
                        <xs:annotation>
                            <xs:documentation>Describes the version of the XML content. By this version the application engineer can manage
                                his approval process.</xs:documentation>
                        </xs:annotation>
                        <xs:attribute name="version" type="xs:string" use="required"/>
                        <xs:attribute name="creationDate" type="xs:dateTime" use="required">
                            <xs:annotation>
                                <xs:documentation>UTC timestamp when Lion configuration has been created.</xs:documentation>
                            </xs:annotation>
                        </xs:attribute>
                        <xs:attribute name="changeDate" type="xs:dateTime" use="required">
                            <xs:annotation>
                                <xs:documentation>UTC timestamp when Lion configuration has been changed last time.</xs:documentation>
                            </xs:annotation>
                        </xs:attribute>
                        <xs:attribute name="changedBy" type="xs:string" use="required">
                            <xs:annotation>
                                <xs:documentation>Name of user who has changed the Lion configuration.</xs:documentation>
                            </xs:annotation>
                        </xs:attribute>
                        <xs:attribute name="description" type="xs:string">
                            <xs:annotation>
                                <xs:documentation>Optional change description.</xs:documentation>
                            </xs:annotation>
                        </xs:attribute>
                    </xs:complexType>
                </xs:element>

                <xs:element name="description" type="cr:descriptionType" minOccurs="0" default=""/>

                <!-- place for the used Lion modules. -->
                <xs:any namespace="##other" minOccurs="0" maxOccurs="unbounded">
                    <xs:annotation>
                        <xs:documentation>Place to insert the top-most configuration of a Lion module (see for example 
                            element 'digitalTwin' in digitalTwinCfg.xsd.</xs:documentation>
                    </xs:annotation>
                </xs:any>
                
            </xs:sequence>

            <xs:attribute name="id" type="cr:optEntityIdType" use="optional" default=""/>
            <xs:attribute name="name" type="cr:optEntityNameType" use="optional" default=""/>
        </xs:complexType>
    </xs:element>
    
</xs:schema>
