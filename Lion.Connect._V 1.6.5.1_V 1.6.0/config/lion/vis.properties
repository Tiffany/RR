# ----------------- List of HMI properties ---------------------------------------------------

# If HMI application should be wait/block during bootup until all connections are available. 
itemManager.expectAllConnections=false

# Address of server HMI
app.serverIpAddr=127.0.0.1

# Printer is connected to the machine (print screen button activation)
printer.enable = false

link.stopReasons = http://localhost:8080/?location=stopReasons&unit=

link.jobActions = http://localhost:8080/?location=jobActions&unit=

# Defines the timeout for the user to select a reason for an unqualified machine
# stop [ms].
stopReason.timeout = 10000

# Defines the timeout for the user to process the requested machine action [ms].
job.actionTimeout = 10000

# Defines how the job-engine shall calculate the effort to execute the actions.
# There are to strategies "QUANTITY" and "CALCULATED_EFFORT". The first count
# counts the actions in the job and the second one uses the attribute "calculatedEffort"
# of each action. Default is CALCULATED_EFFORT.
job.progressStrategy = CALCULATED_EFFORT

# In case of "job.progressStrategy = CALCULATED_EFFORT" this property defines 
# a default effort [s] (0..n) for all actions which hasn't defined the attribute 
# "calculatedEffort". Default is 60s.
job.defaultEffort = 60

# Defines if all actions of a job should be simulated. Simulations means that
# all actions are set to EXECUTED automatically.
job.simulateActions = false

# Defines if the user can select the machine mode by the HMI. There are some
# machines which have a HW button to select the machine mode.
# true: select by HMI
# false: select by HW-button (default)
machine.mode.selectionOnHMI = true

# Last selected operation mode
machine.mode.lastSelectedMode = 3

# Defines if audit trail should also contain page changes.
auditTrail.logPageChange=false

# Remove the touch pointer on the keyboard when entering a password 
touch.removePointer = true

# Optional position of the master page 0. 
# It is possible to set the absolute location and size (pixels) by 
#   'masterPage.0=x,y,width, height' 
# or by selecting the proper screen index (0..n) by
#   'masterPage.0=0'
# In the second case the master page automatically fills up the complete screen space.
# If the HMI supports further master pages then the second one is configured by
#   'masterPage.1=x,y,width,height'
masterPage.0=0,0,1366,768

# Time interval in milliseconds for which each job is displayed while cycling through the
# running jobs in the JobInfo title widget.
widget.jobInfo.refreshInterval=3000

# Port number of the webApp server
web.webPortNumber=8080

# Port number of the diagnosisWebApp server
web.diagnosisPortNumber=8081

# ----------------- Format properties -------------------------------------------------------

# Display of format description and format image in format pages
format.displayInfo = true

# Automatic loading of last loaded format at startup of application
format.autoLoad = true

# Test if the connections state are ok to allowed saving format parameters 
format.save.testConnections = false

# Format management with approvement mechanism
format.approval = true

# ----------------- Panel properties ------------------------------------------

# Width resolution of the touch panel
# 1024 for 4:3 panel
# 1366 for 16:9 panel
machine.config.screen.width = 1366

# Height resolution of the touch panel
# 768 for 4:3 and 16:9 panel
machine.config.screen.height = 768


# ----------------- User manager properties -------------------------------------------------

#Local user handling
userManager.localUserHandling = true

userManager.ldap.linkType=default

# The name of the user for which we never do the authentication by remote server
# (like ActiveDirectory). This prevents unwhised delaye if remote server isn't online.
userManager.emergencyUser = notfalluser

# Timout for automatical logout if there is no user interaction. [ms] (0..n; 0->endless)
userManager.autoLogoutTimeout = 600000

# Defines the minimum user level to quit messages of classification type "CriticalError"
# by HMI. The integer value represents the user level from asphere user manager configuration.
# ATTENTION: If you want to define that for 'userManager.quitMsg.Error' and 'userManager.quitMsg.Warning'
# too then it is necessary to adopt the processing of 'blue HW quit button'. Otherwise
# there is a secuity hole.
userManager.quitMsg.CriticalError = 64

# Defines if the actions to quit a critical are managed by the userright manager or not
# The actions are: additionnal login , additionnal comment, 4 eyes principles, etc ...
# True ... The userright manager actions will be used to quit critical errors (but only for critical errors)
# False ... The basis function with additional comment will be used to quit critical errors
userManager.quitMsg.CriticalError.actions = true

# Defines which users should be suppressed in table of user management page. Use 
# separated list for more names.
userManager.invisibleUsers=logout, dividella, asphere

# Defines which user groups should be suppressed in 'create user' dialog. Use 
# separated list for more names.
userManager.invisibleUserGroups=common,logout

# Set the local caching of domain users
userManager.localCaching = false

# Each button needed for the user management can be setted as visible or invisible
# Button to create users
userManager.btnCreate.visible = true
# Button to delete users 
userManager.btnDelete.visible = true
# Button to reactivate users
userManager.btnReactivate.visible = true
# Button to change passwords
userManager.btnChangePwd.visible = true
# Button to generate user rights export (variant 1)
userManager.btnUserRightsExport1.visible = true
# Button to generate user rights export (variant 2)
userManager.btnUserRightsExport2.visible = false

# Set the list of languages who should be visible in the choose language combobox 
userManager.visibleLanguages = English,German

# Definiert, ob die alte Philosophie (strategy1) der Benutzerberechtigung
# oder die neue Philosophie (strategy2) verwendet wird.
# Bei der neuen Philosophie kann im Falle von Double-Sign die Menge der m\u00f6glichen Benutzergruppen festgelegt werden
# und es kann die elektronische Signatur genutzt werden.
userManager.userRightsStrategy = strategy2

# Defines if the configuration dialog for user rights shall offer the feature 'change comment'
userManager.userRightsConfiguration.additionalComment = true

# Defines if the configuration dialog for user rights shall offer the feature 'additional login of an other user'
userManager.userRightsConfiguration.additionalUser = true

# Defines if the configuration dialog for user rights shall offer the feature 'additional login'
userManager.userRightsConfiguration.additionalLogin = true

# Defines if the common features like 'additional login', '4 eyes principle' and
# 'change comment' can be defined on navigation buttons too. By default these
# features are only available for leaves in the GUI tree and can't be inherited
# from any navigation button.
userManager.userRightsConfiguration.inheritCommonActions=true

# Defines if the configuration dialog for user rights shall offer the feature 'signed electronic record'
userManager.userRightsConfiguration.signedElectronicRecord=false

# Defines the path of the userRights jasper reports
userManager.report.path.userrights = /ch/dividella/app/vis/res/reports/

# Avoid to delete the last admin user
userManager.keepOneUserWithLevel = 8

# Defniert die Benutzerrechte Einstellungen f\u00fcr die page template buttons (printScreen and viewHelp).
# false: die Benutzerrechte k\u00f6nnen f\u00fcr jede Seite eingestellt werden
# true: die Benutzerrechte k\u00f6nnen nur einmal eingestellt werden
userManager.pageTemplate.buttons.setUserrightsOnceOnly = true

# Defines if the configuration dialog should set AuditTrail entries for user rights changes
userManager.userRightsConfiguration.withAuditTrail = true

# Activation of the 21CFR11 
software.options.fda.21cfr11Enabled = true

# Modes of 21CFR11 implementation.
# MODE1:	first implementation as used by Dividella/Seidenader (backwards compatibility)
# MODE2:	new mode according to requirements of Mediseal:
#			1) format-xml-files are signed.
#			2) user account includes first and last name.		
userManager.21CFR11.mode = MODE2

# ----------------- Backup, export destinations & sources -----------------------------------------------

import.format.source.1 = local; D:/FormatImport
import.format.source.2 = usb; E:/

# Allowed source paths for format pictures
# The specified paths and subpaths are allowed to import (copy) files
# Use property name with continous number. Start with 1 (e.g. import.format.pictures.source.1).
# Specify drive name and path, splitted by a semicolon (e.g. local; C:/Temp/Test).
# Use drive names local (for local drive), usb (for usb drive) and network (for external drive).
import.pictures.source.1 = local; D:/FormatPictures
import.pictures.source.2 = usb; F:/

# Destination path for customer backup
customerBackup.destination.1 = local; D:/CustomerBackup
customerBackup.destination.2 = usb; E:/


# Allowed destination paths for manual audit trail export
# The specified paths and ist subpaths are allowed to save file
# Use property name with continous number. Start with 1 (e.g. export.logFiles.destination.1).
# Specify drive name and path, splitted by a semicolon (e.g. local; C:/Temp/Test).
# Use drive names local (for local drive), usb (for usb drive) and ftp (for external drive).
export.logFiles.destination.1 = local; D:/Export
export.logFiles.destination.2 = usb; E:/

# Allowed destination paths for format backup
# The specified paths and ist subpaths are allowed to save file
# Use property name with continous number. Start with 1 (e.g. backup.format.destination.1).
# Specify drive name and path, splitted by a semicolon (e.g. local; C:/Temp/Test).
# Use drive names local (for local drive), usb (for usb drive) and network (for external drive).
backup.format.destination.1 = local; D:/FormatData
backup.format.destination.2 = usb; E:/

# Allowed destination paths for user rights export
# The specified paths and ist subpaths are allowed to save file
# Use property name with continous number. Start with 1 (e.g. export.userrights.destination.1).
# Specify drive name and path, splitted by a semicolon (e.g. local; C:/Temp/Test).
# Use drive names local (for local drive), usb (for usb drive) and network (for external drive).
export.userrights.destination.1 = local; D:/UserRightsPrint
#export.userrights.destination.2 = usb; E:/

# Allowed destination paths for format export
# The specified paths and ist subpaths are allowed to save file
# Use property name with continous number. Start with 1 (e.g. export.format.destination.1).
# Specify drive name and path, splitted by a semicolon (e.g. local; C:/Temp/Test).
# Use drive names local (for local drive), usb (for usb drive) and ftp (for external drive).
export.format.destination.1 = local; D:/FormatPrint
export.format.destination.2 = usb; E:/


# Allowed destination paths for format parts export
# The specified paths and ist subpaths are allowed to save file
# Use property name with continous number. Start with 1 (e.g. export.format.destination.1).
# Specify drive name and path, splitted by a semicolon (e.g. local; C:/Temp/Test).
# Use drive names local (for local drive), usb (for usb drive) and ftp (for external drive).
export.formatParts.destination.1 = local; D:/Export
export.formatParts.destination.2 = usb; E:/

# Allowed destination paths for manual job actions export
# The specified paths and ist subpaths are allowed to save file
# Use property name with continous number. Start with 1 (e.g. export.jobActions.destinationMan.1).
# Specify drive name and path, splitted by a semicolon (e.g. local; C:/Temp/Test).
# Use drive names local (for local drive), usb (for usb drive) and ftp (for external drive).
export.jobActions.destinationMan.1 = local; D:/Export
export.jobActions.destinationMan.2 = usb; E:/

# Allowed destination paths for manual audit trail export
# The specified paths and ist subpaths are allowed to save file
# Use property name with continous number. Start with 1 (e.g. export.auditTrail.destinationMan.1).
# Specify drive name and path, splitted by a semicolon (e.g. local; C:/Temp/Test).
# Use drive names local (for local drive), usb (for usb drive) and ftp (for external drive).
export.auditTrail.destinationMan.1 = local; D:/AuditTrailPrint
export.auditTrail.destinationMan.2 = usb; E:/
# export.auditTrail.destinationMan.3 = ftp; T:/hma

# Destination path for automatic audit trail export
export.auditTrail.destinationAuto = D:/Export

# Cycle time for automatic audit trail export
# Specify a number + time unit (e.g. 5d)
# Time units:
# h ... hours
# d ... days
# M ... months
# m ... minutes
export.auditTrail.cycle = 0d

# Number of max. export files in destination directory for audit trail
# If max. number is reached, the oldest export file will be deleted
export.auditTrail.maxFiles = 100

# Theshold for alarm message for max. files in destination directory for audit trails
# The value indicates left free number of files in percent
export.auditTrail.alarmThreshold = 10

# Allowed destination paths for manual message history export
# The specified paths and ist subpaths are allowed to save file
# Use property name with continous number. Start with 1 (e.g. export.messageHistory.destinationMan.1).
# Specify drive name and path, splitted by a semicolon (e.g. local; C:/Temp/Test).
# Use drive names local (for local drive), usb (for usb drive) and ftp (for external drive).
export.messageHistory.destinationMan.1 = local; D:/MessagePrint
export.messageHistory.destinationMan.2 = usb; E:/
# export.messageHistory.destinationMan.3 = ftp; T:/hma

# Destination path for automatic message history export
export.messageHistory.destinationAuto = D:/Export

# Cycle time for automatic message history export
# Specify a number + time unit (e.g. 5d)
# Time units:
# h ... hours
# d ... days
# M ... months
export.messageHistory.cycle = 0d

# Number of max. export files in destination directory for message history
# If max. number is reached, the oldest export file will be deleted
export.messageHistory.maxFiles = 100

# Theshold for alarm message for max. files in destination directory for message history
# The value indicates left free number of files in percent
export.messageHistory.alarmThreshold = 10

# Destination path for batch report export
export.batchReport.destinationMan.1 = local; D:/BatchReport
export.batchReport.destinationMan.2 = usb; E:/

# Prefix name for the batch report export file
# The complete name is with a time stamp e.g.: batchReport_2013.04.28_10.14 
export.batchReport.fileName.prefix = batchReport

# Usage of the time stamp in the batch report filename
export.batchReport.fileName.timeStamp = true

# Usage of the batch number in the batch report filename
export.batchReport.fileName.batchNr = true

# Formatter for date in the batch report filename
export.batchReport.fileName.dateformatter = yyyy.MM.dd_HH.mm.ss

# Set the visibility (or not) of the pdf preview button
export.btnPdfPreview.visible = true

# Destination folder for continuous audittralil
export.auditcsv.destinationFolder= D:/Export/Audittrail

# Destination folder for continuous audittralil
export.cognex.auditcsv.destinationFolder= D:/Export/CognexAudittrail

# Destination folder for continuous messages
export.msgcsv.destinationFolder= D:/Export/MessageHistory

# ----------------- Specific path and class names -----------------------------------

# Class name for an eventuel AppView class in the custom project
# usefull to call initialisation methods once the bootup sequence is finished.
# Default class name: ch.dividella.vis.ui.AppView
# Customized class name: ch.dividella.app.vis.ui.CustomAppView
appView.className = ch.dividella.app.vis.ui.CustomAppView

# Path of the current message page 
path.page.currentMessage = messages.current.machine.message

# Path of the production overview page (main page)
path.page.productionOverview = production.overview.machine


# ----------------- Multitouch Settings ---------------------------------------------
# Defines if Multitouch is enabled
HMI.MultiTouch.enabled=false
# Width of the Screen (not the application)
HMI.MultiTouch.width=1366
# Height of the Screen (not the application)
HMI.MultiTouch.height=768
# Orientation of the Screen (LANDSCAPE or PORTRAIT)
HMI.MultiTouch.orientation=LANDSCAPE
# Defines if the screen is flipped
HMI.MultiTouch.flipped=false
# activates more logs of the multitouch driver
HMI.MultiTouch.verbose=false


# ----------------- Database ---------------------------------------

# Is file lioncfg.xml encrypted
lioncfg.encrypted=false

# path for batch reports
report.batch.path=./reportData

# database where LMS data is stored on.
# implemented are:
# Javadb ... Apache Derby
# PostgreSQL ... PostgreSQL 11
# SQL-Server ... Microsoft SQL Server 2014
database=Javadb
operationMode.lastSelectedMode = 3
userManager.ldap.kdc =
userManager.ldap.realm =
userManager.ldap.server1 =
userManager.ldap.server2 =
userManager.ldap.ssl = false
userManager.ldap.useSeparateAuth = false
userManager.ldap.userName =
userManager.ldap.password =
userManager.ldap.groupmapping = #admin#admin
userManager.ldap.userdirs =
userManager.ldap.groupdirs =
